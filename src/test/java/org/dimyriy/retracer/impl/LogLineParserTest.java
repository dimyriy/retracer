package org.dimyriy.retracer.impl;

import org.dimyriy.retracer.configuration.Defaults;
import org.dimyriy.retracer.impl.model.LogLine;
import org.dimyriy.retracer.impl.model.LogLineParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

/**
 * @author Dmitrii Bogdanov
 * Created at 10.05.18
 */
@Tag("fast")
class LogLineParserTest {

  @Test
  void tokenizeNormalLogLineShouldReturnCorrectValue() {
    final LogLine line = new LogLineParser(Defaults.READ_TIMESTAMP_FORMAT).parse(
        "2013-10-23T10:12:35.298Z 2013-10-23T10:12:35.300Z eckakaau service3 d6m3shqy->62d45qeh");
    Assertions.assertNotNull(line);
    Assertions.assertEquals("d6m3shqy", line.getCallerSpan());
    Assertions.assertEquals("62d45qeh", line.getSpan());
    Assertions.assertEquals("service3", line.getService());
    Assertions.assertEquals("eckakaau", line.getTrace());
  }
}