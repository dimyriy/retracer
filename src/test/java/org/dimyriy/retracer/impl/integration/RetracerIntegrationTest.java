package org.dimyriy.retracer.impl.integration;

import org.dimyriy.retracer.Main;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Random;

/**
 * @author Dmitrii Bogdanov
 * Created at 13.05.18
 */
class RetracerIntegrationTest {
  private static final Random RANDOM = new Random();

  @Test
  void testE2eSmall() throws IOException, InterruptedException {
    runAndAssertByComparator("src/test/resources/data/logs/small-log.txt", "src/test/resources/data/traces/small-traces.txt");
  }

  @Test
  void testE2eMedium() throws IOException, InterruptedException {
    runAndAssertByComparator("src/test/resources/data/logs/medium-log.txt", "src/test/resources/data/traces/medium-traces.txt");
  }

  @SuppressWarnings("unused")
  void testE2eLarge() throws IOException, InterruptedException {
    runAndAssertByComparator("src/test/resources/data/logs/large-log.txt", "src/test/resources/data/traces/large-traces.txt");
  }

  @SuppressWarnings("SameParameterValue")
  private void runAndAssertByComparator(final String logName, final String traceName) throws IOException, InterruptedException {
    final String filename = Math.abs(RANDOM.nextInt()) + ".json";
    final String logFilename = Objects.requireNonNull(Paths.get(logName)).toString();
    final String traceFilename = Objects.requireNonNull(Paths.get(traceName)).toString();
    final String comparatorJar = Objects.requireNonNull(Paths.get("src/test/java/org/dimyriy/retracer/impl/aux/trace-comparator.jar")).toString();
    Main.main(new String[]{"--write-trace-to-filename", filename, "--read-log-from-filename", logFilename, "--use-multiple-threads"});
    final Process comparator = new ProcessBuilder().command("java", "-jar", comparatorJar, filename, traceFilename).start();
    try (final BufferedReader in = new BufferedReader(new InputStreamReader(comparator.getInputStream()))) {
      comparator.waitFor();
      final String output = in.readLine();
      Files.delete(Paths.get(filename));
      Assertions.assertEquals("Are files equivalent?: true", output);
    }
  }
}
