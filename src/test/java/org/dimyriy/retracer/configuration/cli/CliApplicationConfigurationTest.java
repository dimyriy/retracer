package org.dimyriy.retracer.configuration.cli;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

/**
 * @author Dmitrii Bogdanov
 * Created at 10.05.18
 */
@Tag("fast")
class CliApplicationConfigurationTest {
  private CliApplicationConfiguration cliApplicationConfiguration;

  @BeforeEach
  void setUp() {
    cliApplicationConfiguration = new CliApplicationConfiguration();
  }

  @Test
  void testSettingBothOutputStatisticsToStderrAndFilenameThrowsInvalidException() {
    Assertions.assertThrows(CliApplicationConfiguration.InvalidException.class,
                            () -> cliApplicationConfiguration.parseCommandLine(new String[]{"--output-statistics-to-filename",
                                                                                            "dummy",
                                                                                            "--output-statistics-to-stderr"}));
  }

  @Test
  void testSettingBothOutputToStdoutAndFilenameThrowsInvalidException() {
    Assertions.assertThrows(CliApplicationConfiguration.InvalidException.class,
                            () -> cliApplicationConfiguration.parseCommandLine(new String[]{"--write-trace-to-filename", "dummy", "--write-trace-to-stdout"}));
  }

  @Test
  void testSettingBothInputFromStdinAndFilenameThrowsInvalidException() {
    Assertions.assertThrows(CliApplicationConfiguration.InvalidException.class,
                            () -> cliApplicationConfiguration.parseCommandLine(new String[]{"--read-log-from-filename", "dummy", "--read-log-from-stdin"}));
  }

  @Test
  void testSettingLogFilenameWithoutArgumentThrowsInvalidException() {
    Assertions.assertThrows(CliApplicationConfiguration.InvalidException.class,
                            () -> cliApplicationConfiguration.parseCommandLine(new String[]{"--read-log-from-filename"}));
  }

  @Test
  void testSettingTraceFilenameWithoutArgumentThrowsInvalidException() {
    Assertions.assertThrows(CliApplicationConfiguration.InvalidException.class,
                            () -> cliApplicationConfiguration.parseCommandLine(new String[]{"--write-trace-to-filename"}));
  }

  @Test
  void testSettingEncodingWithoutArgumentThrowsInvalidException() {
    Assertions.assertThrows(CliApplicationConfiguration.InvalidException.class, () -> cliApplicationConfiguration.parseCommandLine(new String[]{"--encoding"}));
  }

  @Test
  void testSettingLogTimestampFormatWithoutArgumentThrowsInvalidException() {
    Assertions.assertThrows(CliApplicationConfiguration.InvalidException.class,
                            () -> cliApplicationConfiguration.parseCommandLine(new String[]{"--read-log-timestamp-format"}));
  }

  @Test
  void testSettingTraceTimestampFormatWithoutArgumentThrowsInvalidException() {
    Assertions.assertThrows(CliApplicationConfiguration.InvalidException.class,
                            () -> cliApplicationConfiguration.parseCommandLine(new String[]{"--write-trace-timestamp-format"}));
  }

  @Test
  void testSettingOrphanedLinesTimeoutMsWithoutArgumentThrowsInvalidException() {
    Assertions.assertThrows(CliApplicationConfiguration.InvalidException.class,
                            () -> cliApplicationConfiguration.parseCommandLine(new String[]{"--drop-orphaned-lines-timeout-ms"}));
  }

  @Test
  void testSettingUnknownOptionThrowsInvalidException() {
    Assertions.assertThrows(CliApplicationConfiguration.InvalidException.class,
                            () -> cliApplicationConfiguration.parseCommandLine(new String[]{"--absolutely-unknown-option"}));
  }
}
