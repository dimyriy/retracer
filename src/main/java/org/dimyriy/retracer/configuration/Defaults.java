package org.dimyriy.retracer.configuration;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.format.DateTimeFormatter;

/**
 * @author Dmitrii Bogdanov
 * Created at 09.05.18
 */
public class Defaults {
  public static final Charset ENCODING = StandardCharsets.UTF_8;
  public static final boolean READ_FROM_STDIN = false;
  public static final String READ_FROM_FILENAME = "application.log";
  public static final boolean WRITE_TO_STDOUT = false;
  public static final String WRITE_TO_FILENAME = "trace.json";
  public static final boolean WRITE_STATISTICS_TO_STDERR = false;
  public static final String WRITE_STATISTICS_TO_FILENAME = null;
  public static final boolean WRITE_RUNTIME_STATISTICS = false;
  public static final boolean WRITE_TRACES_STATISTICS = false;
  public static final String READ_TIMESTAMP_FORMAT_PATTERN = "yyyy-M-d'T'H:mm:ss[.SSS]VV";
  public static final String WRITE_TIMESTAMP_FORMAT_PATTERN = "yyyy-M-d'T'H:mm:ss[.SSS]VV";
  public static final DateTimeFormatter READ_TIMESTAMP_FORMAT = DateTimeFormatter.ofPattern(READ_TIMESTAMP_FORMAT_PATTERN);
  public static final DateTimeFormatter WRITE_TIMESTAMP_FORMAT = DateTimeFormatter.ofPattern(WRITE_TIMESTAMP_FORMAT_PATTERN);
  public static final long WRITE_STATISTICS_INTERVAL_MS = 1000L;
  public static final boolean USE_MULTIPLE_THREADS = false;
  public static final long DROP_ORPHANED_LINES_TIMEOUT_MS = 120000L;
}
