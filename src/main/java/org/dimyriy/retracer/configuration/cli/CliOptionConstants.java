package org.dimyriy.retracer.configuration.cli;

class CliOptionConstants {
  static final String ENCODING = "encoding";
  static final String APPLICATION_NAME = "retracer";
  static final String HELP = "h";
  static final String HELP_LONG = "help";
  static final String VERSION = "v";
  static final String VERSION_LONG = "version";
  static final String READ_TIMESTAMP_FORMAT = "read-log-timestamp-format";
  static final String READ_FROM_STDIN = "read-log-from-stdin";
  static final String READ_FROM_FILENAME = "read-log-from-filename";
  static final String WRITE_TIMESTAMP_FORMAT = "write-trace-timestamp-format";
  static final String WRITE_TO_STDOUT = "write-trace-to-stdout";
  static final String WRITE_TO_FILENAME = "write-trace-to-filename";
  static final String WRITE_STATISTICS_TO_STDERR = "output-statistics-to-stderr";
  static final String WRITE_STATISTICS_TO_FILENAME = "output-statistics-to-filename";
  static final String WRITE_STATISTICS_INTERVAL_MS = "output-statistics-interval-ms";
  static final String WRITE_RUNTIME_STATISTICS = "enable-runtime-statistics";
  static final String WRITE_TRACES_STATISTICS = "enable-traces-statistics";
  static final String USE_MULTIPLE_THREADS = "use-multiple-threads";
  static final String DROP_ORPHANED_LINES_TIMEOUT_MS = "drop-orphaned-lines-timeout-ms";
}
