package org.dimyriy.retracer.configuration.cli;

import org.apache.commons.cli.*;
import org.dimyriy.retracer.Main;
import org.dimyriy.retracer.configuration.ApplicationConfiguration;
import org.dimyriy.retracer.configuration.Defaults;
import org.dimyriy.retracer.configuration.api.*;
import org.dimyriy.retracer.configuration.impl.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import static org.dimyriy.retracer.configuration.cli.CliOptionConstants.*;

/**
 * @author Dmitrii Bogdanov
 * Created at 09.05.18
 */
public class CliApplicationConfiguration {
  private static final Map<String, Function<String, Object>> UNSUPPORTED_OPTION_TYPES_INSTANTIATOR = new ConcurrentHashMap<>();
  private static final int HELP_WIDTH = 154;

  static {
    UNSUPPORTED_OPTION_TYPES_INSTANTIATOR.put(ENCODING, s -> {
      try {
        return Charset.forName(s);
      } catch (UnsupportedCharsetException e) {
        throw new InvalidException("Unsupported charset: " + e.getMessage(), e);
      }
    });
    UNSUPPORTED_OPTION_TYPES_INSTANTIATOR.put(READ_TIMESTAMP_FORMAT, s -> {
      try {
        return DateTimeFormatter.ofPattern(s);
      } catch (Exception e) {
        throw new InvalidException("Cannot create date formatter for pattern: " + s, e);
      }
    });
    UNSUPPORTED_OPTION_TYPES_INSTANTIATOR.put(WRITE_TIMESTAMP_FORMAT, s -> {
      try {
        return DateTimeFormatter.ofPattern(s);
      } catch (Exception e) {
        throw new InvalidException("Cannot create date formatter for pattern: " + s, e);
      }
    });
  }

  private final Options options = createCliOptions();

  public ApplicationConfiguration parseCli(@Nonnull final String[] args) {
    return buildApplicationConfiguration(parseCommandLine(args));
  }

  public void printUsage() {
    final HelpFormatter helpFormatter = new HelpFormatter();
    helpFormatter.setWidth(HELP_WIDTH);
    helpFormatter.setOptionComparator(null);
    helpFormatter.printHelp(APPLICATION_NAME + " --" + CliOptionConstants.WRITE_STATISTICS_TO_FILENAME + " stats.txt", options);
  }

  private ApplicationConfiguration buildApplicationConfiguration(@Nonnull final CommandLine cli) throws InvalidException {
    try {
      return new ApplicationConfiguration(createInputReaderConfiguration(cli),
                                          createOutputWriterConfiguration(cli),
                                          createStatsWriterConfiguration(cli),
                                          createLogicConfiguration(cli),
                                          createRuntimeConfiguration(cli));
    } catch (ParseException e) {
      throw new InvalidException(e.getMessage(), e);
    }
  }

  CommandLine parseCommandLine(@Nonnull final String[] args) {
    final CommandLine cliArgs;
    try {
      cliArgs = new DefaultParser().parse(options, args);
    } catch (ParseException e) {
      throw new InvalidException(e.getMessage(), e);
    }
    if (cliArgs.hasOption(CliOptionConstants.VERSION)) {
      printVersionAndExit();
    }
    if (cliArgs.hasOption(CliOptionConstants.HELP)) {
      printHelpAndExit();
    }
    return cliArgs;
  }

  private InputReaderConfiguration createInputReaderConfiguration(@Nonnull final CommandLine cli) throws ParseException {
    return new InputReaderConfigurationImpl(getOptionValueOrDefault(cli, READ_FROM_STDIN, Defaults.READ_FROM_STDIN),
                                            getOptionValueOrDefault(cli, READ_FROM_FILENAME, Defaults.READ_FROM_FILENAME),
                                            getOptionValueOrDefault(cli, READ_TIMESTAMP_FORMAT, Defaults.READ_TIMESTAMP_FORMAT),
                                            getOptionValueOrDefault(cli, ENCODING, Defaults.ENCODING));
  }

  private OutputWriterConfiguration createOutputWriterConfiguration(@Nonnull final CommandLine cli) throws ParseException {
    return new OutputWriterConfigurationImpl(getOptionValueOrDefault(cli, WRITE_TO_STDOUT, Defaults.WRITE_TO_STDOUT),
                                             getOptionValueOrDefault(cli, WRITE_TO_FILENAME, Defaults.WRITE_TO_FILENAME),
                                             getOptionValueOrDefault(cli, WRITE_TIMESTAMP_FORMAT, Defaults.WRITE_TIMESTAMP_FORMAT),
                                             getOptionValueOrDefault(cli, ENCODING, Defaults.ENCODING));
  }

  private StatisticsWriterConfiguration createStatsWriterConfiguration(@Nonnull final CommandLine cli) throws ParseException {
    return new StatisticsWriterConfigurationImpl(getOptionValueOrDefault(cli, WRITE_STATISTICS_TO_STDERR, Defaults.WRITE_STATISTICS_TO_STDERR),
                                                 getOptionValueOrDefault(cli, WRITE_STATISTICS_TO_FILENAME, Defaults.WRITE_STATISTICS_TO_FILENAME),
                                                 getOptionValueOrDefault(cli, WRITE_RUNTIME_STATISTICS, Defaults.WRITE_RUNTIME_STATISTICS),
                                                 getOptionValueOrDefault(cli, WRITE_TRACES_STATISTICS, Defaults.WRITE_TRACES_STATISTICS),
                                                 getOptionValueOrDefault(cli, WRITE_STATISTICS_INTERVAL_MS, Defaults.WRITE_STATISTICS_INTERVAL_MS));
  }

  private LogicConfiguration createLogicConfiguration(@Nonnull final CommandLine cli) throws ParseException {
    return new LogicConfigurationImpl(getOptionValueOrDefault(cli, DROP_ORPHANED_LINES_TIMEOUT_MS, Defaults.DROP_ORPHANED_LINES_TIMEOUT_MS));
  }

  private RuntimeConfiguration createRuntimeConfiguration(@Nonnull final CommandLine cli) throws ParseException {
    return new RuntimeConfigurationImpl(getOptionValueOrDefault(cli, USE_MULTIPLE_THREADS, Defaults.USE_MULTIPLE_THREADS));
  }

  private void printVersionAndExit() {
    System.out.println(APPLICATION_NAME + " " + Main.class.getPackage().getImplementationVersion());
    System.exit(0);
  }

  private void printHelpAndExit() {
    printUsage();
    System.exit(0);
  }

  @SuppressWarnings("unchecked")
  private static <T> T getOptionValueOrDefault(@Nonnull final CommandLine cli,
                                               @Nonnull final String optionName,
                                               @Nullable final T defaultValue) throws ParseException {
    final Object parsedOptionValue;
    if (defaultValue instanceof Boolean) {
      parsedOptionValue = cli.hasOption(optionName);
    } else {
      if (cli.hasOption(optionName)) {
        if (UNSUPPORTED_OPTION_TYPES_INSTANTIATOR.containsKey(optionName)) {
          parsedOptionValue = UNSUPPORTED_OPTION_TYPES_INSTANTIATOR.get(optionName).apply(cli.getOptionValue(optionName));
        } else {
          parsedOptionValue = cli.getParsedOptionValue(optionName);
        }
      } else {
        parsedOptionValue = null;
      }
    }
    return parsedOptionValue != null ? (T) parsedOptionValue : defaultValue;
  }

  private static Options createCliOptions() {
    final Options options = new Options();
    addLogTimestampFormatOption(options);
    addTraceTimestampFormatOption(options);
    addDropOrphanedLinesTimeoutOption(options);
    addEncodingOption(options);
    addUseMultipleThreadsOption(options);
    addReadInputOptionGroup(options);
    addWriteOutputOptionGroup(options);
    addWriteStatisticsOptionGroup(options);
    addOutputStatisticsIntervalMs(options);
    addEnableRuntimeStatistics(options);
    addEnableTracesStatistics(options);
    addHelpOption(options);
    addVersionOption(options);
    return options;
  }

  private static void addLogTimestampFormatOption(final Options options) {
    options.addOption(Option.builder()
                            .longOpt(CliOptionConstants.READ_TIMESTAMP_FORMAT)
                            .hasArg()
                            .type(SimpleDateFormat.class)
                            .desc(buildDescriptionWithDefault("Log timestamp format (DateTimeFormatter).", Defaults.READ_TIMESTAMP_FORMAT_PATTERN))
                            .build());
  }

  private static void addTraceTimestampFormatOption(final Options options) {
    options.addOption(Option.builder()
                            .longOpt(CliOptionConstants.WRITE_TIMESTAMP_FORMAT)
                            .hasArg()
                            .type(String.class)
                            .desc(buildDescriptionWithDefault("TraceRoot timestamp format (DateTimeFormatter).", Defaults.WRITE_TIMESTAMP_FORMAT_PATTERN))
                            .build());
  }

  private static void addDropOrphanedLinesTimeoutOption(final Options options) {
    options.addOption(Option.builder()
                            .longOpt(CliOptionConstants.DROP_ORPHANED_LINES_TIMEOUT_MS)
                            .hasArg()
                            .type(Long.class)
                            .desc(buildDescriptionWithDefault("Timeout after which orphaned log lines are being dropped.",
                                                              "" + Defaults.DROP_ORPHANED_LINES_TIMEOUT_MS))
                            .build());
  }

  private static void addOutputStatisticsIntervalMs(final Options options) {
    options.addOption(Option.builder()
                            .longOpt(CliOptionConstants.WRITE_STATISTICS_INTERVAL_MS)
                            .hasArg()
                            .type(Long.class)
                            .desc(buildDescriptionWithDefault("Write statistics every N ms.", "" + Defaults.WRITE_STATISTICS_INTERVAL_MS))
                            .build());
  }

  private static void addEnableRuntimeStatistics(final Options options) {
    options.addOption(Option.builder()
                            .longOpt(CliOptionConstants.WRITE_RUNTIME_STATISTICS)
                            .type(Boolean.class)
                            .desc(buildDescriptionWithDefault("Dump runtime statistics.", "" + Defaults.WRITE_RUNTIME_STATISTICS))
                            .build());
  }

  private static void addEnableTracesStatistics(final Options options) {
    options.addOption(Option.builder()
                            .longOpt(CliOptionConstants.WRITE_TRACES_STATISTICS)
                            .type(Boolean.class)
                            .desc(buildDescriptionWithDefault("Dump statistics about traces.", "" + Defaults.WRITE_RUNTIME_STATISTICS))
                            .build());
  }

  private static void addEncodingOption(final Options options) {
    options.addOption(Option.builder()
                            .longOpt(CliOptionConstants.ENCODING)
                            .hasArg()
                            .type(Charset.class)
                            .desc(buildDescriptionWithDefault("File encoding.", Defaults.ENCODING.name()))
                            .build());
  }

  private static void addUseMultipleThreadsOption(final Options options) {
    options.addOption(Option.builder()
                            .longOpt(CliOptionConstants.USE_MULTIPLE_THREADS)
                            .type(Boolean.class)
                            .desc(buildDescriptionWithDefault("Flag marking whether to use multiple threads or not.", "" + Defaults.USE_MULTIPLE_THREADS))
                            .build());
  }

  private static void addReadInputOptionGroup(final Options options) {
    final OptionGroup readLogsGroup = new OptionGroup();
    readLogsGroup.addOption(Option.builder()
                                  .longOpt(CliOptionConstants.READ_FROM_STDIN)
                                  .type(Boolean.class)
                                  .desc(buildDescriptionWithDefault("Flag marking whether logs should be read from standard input or not.",
                                                                    "" + Defaults.READ_FROM_STDIN))
                                  .build());
    readLogsGroup.addOption(Option.builder()
                                  .longOpt(CliOptionConstants.READ_FROM_FILENAME)
                                  .hasArg()
                                  .type(String.class)
                                  .desc(buildDescriptionWithDefault("Filename to read logs to parse from.", Defaults.READ_FROM_FILENAME))
                                  .build());
    options.addOptionGroup(readLogsGroup);
  }

  private static void addWriteOutputOptionGroup(final Options options) {
    final OptionGroup writeTracesGroup = new OptionGroup();
    writeTracesGroup.addOption(Option.builder()
                                     .longOpt(CliOptionConstants.WRITE_TO_STDOUT)
                                     .type(Boolean.class)
                                     .desc(buildDescriptionWithDefault("Flag marking whether traces json should be written to standard output or not.",
                                                                       "" + Defaults.WRITE_TO_STDOUT))
                                     .build());
    writeTracesGroup.addOption(Option.builder()
                                     .longOpt(CliOptionConstants.WRITE_TO_FILENAME)
                                     .hasArg()
                                     .type(String.class)
                                     .desc(buildDescriptionWithDefault("Filename to write traces JSON to.", Defaults.WRITE_TO_FILENAME))
                                     .build());
    options.addOptionGroup(writeTracesGroup);
  }

  private static void addWriteStatisticsOptionGroup(final Options options) {
    final OptionGroup writeStatisticsGroup = new OptionGroup();
    writeStatisticsGroup.addOption(Option.builder()
                                         .longOpt(CliOptionConstants.WRITE_STATISTICS_TO_STDERR)
                                         .type(Boolean.class)
                                         .desc(buildDescriptionWithDefault("Flag marking whether execution stats should be written to standard output or not.",
                                                                           "" + Defaults.WRITE_STATISTICS_TO_STDERR))
                                         .build());
    writeStatisticsGroup.addOption(Option.builder()
                                         .longOpt(CliOptionConstants.WRITE_STATISTICS_TO_FILENAME)
                                         .type(String.class)
                                         .desc(buildDescriptionWithDefault("Filename to write execution stats to.", Defaults.WRITE_STATISTICS_TO_FILENAME))
                                         .build());
    options.addOptionGroup(writeStatisticsGroup);
  }

  private static void addHelpOption(final Options options) {
    options.addOption(Option.builder(HELP).longOpt(CliOptionConstants.HELP_LONG).desc("Print this message.").build());
  }

  private static void addVersionOption(final Options options) {
    options.addOption(Option.builder(VERSION).longOpt(CliOptionConstants.VERSION_LONG).desc("Print version.").build());
  }

  private static String buildDescriptionWithDefault(@Nonnull final String description, @Nullable final String defaultValue) {
    final String defaultValueString;
    if (null == defaultValue) {
      defaultValueString = "No default value.";
    } else if ("true".equals(defaultValue)) {
      defaultValueString = "Set by default.";
    } else if ("false".equals(defaultValue)) {
      defaultValueString = "Unset by default.";
    } else {
      defaultValueString = "Default: '" + defaultValue + "'.";
    }
    return description + " " + defaultValueString;
  }

  public static class InvalidException extends RuntimeException {
    InvalidException(@Nonnull final String message, @Nonnull final Throwable cause) {
      super(message, cause);
    }
  }
}
