package org.dimyriy.retracer.configuration.impl;

import org.dimyriy.retracer.configuration.api.LogicConfiguration;

/**
 * @author Dmitrii Bogdanov
 * Created at 10.05.18
 */
public class LogicConfigurationImpl implements LogicConfiguration {
  private final long dropOrphanedLinesAfterTimeoutMs;

  public LogicConfigurationImpl(final long dropOrphanedLinesAfterTimeoutMs) {
    this.dropOrphanedLinesAfterTimeoutMs = dropOrphanedLinesAfterTimeoutMs;
  }

  @Override
  public long dropOrphanedLinesAfterTimeoutMs() {
    return dropOrphanedLinesAfterTimeoutMs;
  }
}
