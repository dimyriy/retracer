package org.dimyriy.retracer.configuration.impl;

import org.dimyriy.retracer.configuration.api.OutputWriterConfiguration;
import org.dimyriy.retracer.impl.io.Writer;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.nio.charset.Charset;
import java.time.format.DateTimeFormatter;

/**
 * @author Dmitrii Bogdanov
 * Created at 10.05.18
 */
public class OutputWriterConfigurationImpl implements OutputWriterConfiguration {
  private final DateTimeFormatter dateFormat;
  private final boolean writeOutputToStdout;
  private final String filename;
  private final Charset charset;
  private volatile Writer writer;

  public OutputWriterConfigurationImpl(final boolean writeOutputToStdout, @Nullable final String filename, @Nonnull final DateTimeFormatter dateFormat,
                                       @Nonnull final Charset charset) {
    if (!writeOutputToStdout && filename == null) {
      throw new IllegalArgumentException("Either output to stdout or filename should be configured");
    }
    this.dateFormat = dateFormat;
    this.writeOutputToStdout = writeOutputToStdout;
    this.filename = filename;
    this.charset = charset;
  }

  @Override
  public boolean isWriteOutputToFile() {
    return !writeOutputToStdout;
  }

  @Override
  public boolean isWriteOutputToStdout() {
    return writeOutputToStdout;
  }

  @Override
  public String writeOutputToFilename() {
    return filename;
  }

  @Override
  public DateTimeFormatter threadSafeOutputDateFormatter() {
    return dateFormat;
  }

  @Override
  public boolean isBoundToWriter() {
    return this.writer == null;
  }

  @Override
  public void bindToWriter(@Nullable final Writer writer) {
    if (this.writer != null && writer != null) {
      throw new AlreadyBoundException();
    }
    this.writer = writer;
  }

  @Override
  public Charset getOutputCharset() {
    return charset;
  }

  class AlreadyBoundException extends RuntimeException {
    AlreadyBoundException() {
      super("Writer already bound");
    }
  }
}
