package org.dimyriy.retracer.configuration.impl;

import org.dimyriy.retracer.configuration.api.InputReaderConfiguration;
import org.dimyriy.retracer.impl.io.Reader;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.nio.charset.Charset;
import java.time.format.DateTimeFormatter;

/**
 * @author Dmitrii Bogdanov
 * Created at 10.05.18
 */
public class InputReaderConfigurationImpl implements InputReaderConfiguration {
  private final DateTimeFormatter dateFormat;
  private final boolean readInputFromStdin;
  private final String filename;
  private final Charset charset;
  private volatile Reader reader;

  public InputReaderConfigurationImpl(final boolean readInputFromStdin, @Nullable final String filename, @Nonnull final DateTimeFormatter dateFormat,
                                      @Nonnull final Charset charset) {
    if (!readInputFromStdin && filename == null) {
      throw new IllegalArgumentException("Either input from stdin or filename should be configured");
    }
    this.readInputFromStdin = readInputFromStdin;
    this.dateFormat = dateFormat;
    this.filename = filename;
    this.charset = charset;
  }

  @Override
  public boolean isReadFromFile() {
    return !readInputFromStdin;
  }

  @Override
  public boolean isReadInputFromStdin() {
    return readInputFromStdin;
  }

  @Override
  public String inputFilename() {
    return filename;
  }

  @Override
  public DateTimeFormatter threadSafeInputDateFormatter() {
    return dateFormat;
  }

  @Override
  public boolean isBoundToReader() {
    return reader != null;
  }

  @Override
  public void bindToReader(@Nullable final Reader reader) {
    if (this.reader != null && reader != null) {
      throw new AlreadyBoundException();
    }
    this.reader = reader;
  }

  @Override
  public Charset getInputCharset() {
    return charset;
  }

  class AlreadyBoundException extends RuntimeException {
    AlreadyBoundException() {
      super("Reader already bound");
    }
  }
}
