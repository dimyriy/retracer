package org.dimyriy.retracer.configuration.impl;

import org.dimyriy.retracer.configuration.api.RuntimeConfiguration;

/**
 * @author Dmitrii Bogdanov
 * Created at 10.05.18
 */
public class RuntimeConfigurationImpl implements RuntimeConfiguration {
  private final boolean useMultipleThreads;

  public RuntimeConfigurationImpl(final boolean useMultipleThreads) {
    this.useMultipleThreads = useMultipleThreads;
  }

  @Override
  public boolean useMultipleThreads() {
    return useMultipleThreads;
  }
}
