package org.dimyriy.retracer.configuration.impl;

import org.dimyriy.retracer.configuration.api.StatisticsWriterConfiguration;
import org.dimyriy.retracer.impl.io.StatWriter;

import javax.annotation.Nullable;

/**
 * @author Dmitrii Bogdanov
 * Created at 10.05.18
 */
public class StatisticsWriterConfigurationImpl implements StatisticsWriterConfiguration {
  private final boolean writeStatisticsToStderr;
  private final boolean writeTracesStatistics;
  private final boolean writeRuntimeStatistics;
  private final String filename;
  private final long outputStatisticsIntervalMs;
  private volatile StatWriter statWriter;

  public StatisticsWriterConfigurationImpl(final boolean writeStatisticsToStderr,
                                           @Nullable final String filename,
                                           final boolean writeTracesStatistics,
                                           final boolean writeRuntimeStatistics,
                                           final long outputStatisticsIntervalMs) {
    this.writeStatisticsToStderr = writeStatisticsToStderr;
    this.filename = filename;
    this.outputStatisticsIntervalMs = outputStatisticsIntervalMs;
    this.writeTracesStatistics = writeTracesStatistics;
    this.writeRuntimeStatistics = writeRuntimeStatistics;
  }

  @Override
  public boolean isWriteStatisticsToFile() {
    return filename != null;
  }

  @Override
  public boolean isWriteStatisticsToStderr() {
    return writeStatisticsToStderr;
  }

  @Override
  public boolean isRuntimeStatisticsWriterEnabled() {
    return writeRuntimeStatistics;
  }

  @Override
  public boolean isTracesStatisticsWriterEnabled() {
    return writeTracesStatistics;
  }

  @Override
  public String writeStatisticsToFilename() {
    return filename;
  }

  @Override
  public boolean isBoundToStatisticsWriter() {
    return this.statWriter != null;
  }

  @Override
  public void bindToStatisticsWriter(@Nullable final StatWriter statWriter) {
    if (this.statWriter != null && statWriter != null) {
      throw new AlreadyBoundException();
    }
    this.statWriter = statWriter;
  }

  @Override
  public long outputStatisticsIntervalMs() {
    return outputStatisticsIntervalMs;
  }

  class AlreadyBoundException extends RuntimeException {
    AlreadyBoundException() {
      super("StatWriter already bound");
    }
  }
}
