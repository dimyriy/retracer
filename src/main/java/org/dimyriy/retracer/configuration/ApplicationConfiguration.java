package org.dimyriy.retracer.configuration;

import org.dimyriy.retracer.configuration.api.*;
import org.dimyriy.retracer.impl.io.Reader;
import org.dimyriy.retracer.impl.io.StatWriter;
import org.dimyriy.retracer.impl.io.Writer;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.nio.charset.Charset;
import java.time.format.DateTimeFormatter;

/**
 * @author Dmitrii Bogdanov
 * Created at 09.05.2018
 */
public class ApplicationConfiguration implements InputReaderConfiguration,
                                                 OutputWriterConfiguration,
                                                 StatisticsWriterConfiguration,
                                                 LogicConfiguration,
                                                 RuntimeConfiguration {
  private final InputReaderConfiguration inputReaderConfiguration;
  private final OutputWriterConfiguration outputWriterConfiguration;
  private final StatisticsWriterConfiguration statisticsWriterConfiguration;
  private final LogicConfiguration logicConfiguration;
  private final RuntimeConfiguration runtimeConfiguration;

  public ApplicationConfiguration(@Nonnull final InputReaderConfiguration inputReaderConfiguration,
                                  @Nonnull final OutputWriterConfiguration outputWriterConfiguration,
                                  @Nonnull final StatisticsWriterConfiguration statisticsWriterConfiguration,
                                  @Nonnull final LogicConfiguration logicConfiguration,
                                  @Nonnull final RuntimeConfiguration runtimeConfiguration) {
    this.inputReaderConfiguration = inputReaderConfiguration;
    this.outputWriterConfiguration = outputWriterConfiguration;
    this.statisticsWriterConfiguration = statisticsWriterConfiguration;
    this.logicConfiguration = logicConfiguration;
    this.runtimeConfiguration = runtimeConfiguration;
  }

  @Override
  public Charset getInputCharset() {
    return inputReaderConfiguration.getInputCharset();
  }

  @Override
  public boolean isReadFromFile() {
    return inputReaderConfiguration.isReadFromFile();
  }

  @Override
  public boolean isReadInputFromStdin() {
    return inputReaderConfiguration.isReadInputFromStdin();
  }

  @Override
  public String inputFilename() {
    return inputReaderConfiguration.inputFilename();
  }

  @Override
  public DateTimeFormatter threadSafeInputDateFormatter() {
    return inputReaderConfiguration.threadSafeInputDateFormatter();
  }

  @Override
  public boolean isBoundToReader() {
    return inputReaderConfiguration.isBoundToReader();
  }

  @Override
  public void bindToReader(@Nullable final Reader reader) {
    inputReaderConfiguration.bindToReader(reader);
  }

  @Override
  public Charset getOutputCharset() {
    return outputWriterConfiguration.getOutputCharset();
  }

  @Override
  public boolean isWriteOutputToFile() {
    return outputWriterConfiguration.isWriteOutputToFile();
  }

  @Override
  public boolean isWriteOutputToStdout() {
    return outputWriterConfiguration.isWriteOutputToStdout();
  }

  @Override
  public String writeOutputToFilename() {
    return outputWriterConfiguration.writeOutputToFilename();
  }

  @Override
  public DateTimeFormatter threadSafeOutputDateFormatter() {
    return outputWriterConfiguration.threadSafeOutputDateFormatter();
  }

  @Override
  public boolean isBoundToWriter() {
    return outputWriterConfiguration.isBoundToWriter();
  }

  @Override
  public void bindToWriter(@Nullable final Writer writer) {
    outputWriterConfiguration.bindToWriter(writer);
  }

  @Override
  public long dropOrphanedLinesAfterTimeoutMs() {
    return logicConfiguration.dropOrphanedLinesAfterTimeoutMs();
  }

  @Override
  public boolean isWriteStatisticsToFile() {
    return statisticsWriterConfiguration.isWriteStatisticsToFile();
  }

  @Override
  public boolean isWriteStatisticsToStderr() {
    return statisticsWriterConfiguration.isWriteStatisticsToStderr();
  }

  @Override
  public boolean isRuntimeStatisticsWriterEnabled() {
    return statisticsWriterConfiguration.isRuntimeStatisticsWriterEnabled();
  }

  @Override
  public boolean isTracesStatisticsWriterEnabled() {
    return statisticsWriterConfiguration.isTracesStatisticsWriterEnabled();
  }

  @Override
  public long outputStatisticsIntervalMs() {
    return statisticsWriterConfiguration.outputStatisticsIntervalMs();
  }

  @Override
  public String writeStatisticsToFilename() {
    return statisticsWriterConfiguration.writeStatisticsToFilename();
  }

  @Override
  public boolean isBoundToStatisticsWriter() {
    return statisticsWriterConfiguration.isBoundToStatisticsWriter();
  }

  @Override
  public void bindToStatisticsWriter(@Nullable final StatWriter statWriter) {
    statisticsWriterConfiguration.bindToStatisticsWriter(statWriter);
  }

  @Override
  public boolean useMultipleThreads() {
    return runtimeConfiguration.useMultipleThreads();
  }

  public int concurrencyLevel() {
    return runtimeConfiguration.useMultipleThreads() ? Runtime.getRuntime().availableProcessors() : 1;
  }
}
