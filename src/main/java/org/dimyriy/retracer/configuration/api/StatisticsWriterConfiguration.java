package org.dimyriy.retracer.configuration.api;

import org.dimyriy.retracer.impl.io.StatWriter;

import javax.annotation.Nullable;

/**
 * @author Dmitrii Bogdanov
 * Created at 10.05.18
 */
public interface StatisticsWriterConfiguration {
  boolean isWriteStatisticsToFile();

  boolean isWriteStatisticsToStderr();

  boolean isRuntimeStatisticsWriterEnabled();

  boolean isTracesStatisticsWriterEnabled();

  @Nullable
  String writeStatisticsToFilename();

  boolean isBoundToStatisticsWriter();

  void bindToStatisticsWriter(@Nullable final StatWriter statWriter);

  long outputStatisticsIntervalMs();
}
