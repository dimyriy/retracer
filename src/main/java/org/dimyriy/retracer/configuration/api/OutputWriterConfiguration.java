package org.dimyriy.retracer.configuration.api;

import org.dimyriy.retracer.impl.io.Writer;

import javax.annotation.Nullable;
import java.nio.charset.Charset;
import java.time.format.DateTimeFormatter;

/**
 * @author Dmitrii Bogdanov
 * Created at 10.05.18
 */
public interface OutputWriterConfiguration {
  Charset getOutputCharset();

  boolean isWriteOutputToFile();

  boolean isWriteOutputToStdout();

  String writeOutputToFilename();

  DateTimeFormatter threadSafeOutputDateFormatter();

  boolean isBoundToWriter();

  void bindToWriter(@Nullable Writer writer);
}
