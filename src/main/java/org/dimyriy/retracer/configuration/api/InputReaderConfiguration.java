package org.dimyriy.retracer.configuration.api;

import org.dimyriy.retracer.impl.io.Reader;

import javax.annotation.Nullable;
import java.nio.charset.Charset;
import java.time.format.DateTimeFormatter;

/**
 * @author Dmitrii Bogdanov
 * Created at 10.05.18
 */
public interface InputReaderConfiguration {
  Charset getInputCharset();

  boolean isReadFromFile();

  boolean isReadInputFromStdin();

  String inputFilename();

  DateTimeFormatter threadSafeInputDateFormatter();

  boolean isBoundToReader();

  void bindToReader(@Nullable Reader reader);
}
