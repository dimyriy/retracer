package org.dimyriy.retracer.configuration.api;

/**
 * @author Dmitrii Bogdanov
 * Created at 10.05.18
 */
public interface LogicConfiguration {
  long dropOrphanedLinesAfterTimeoutMs();
}
