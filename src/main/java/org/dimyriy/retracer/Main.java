package org.dimyriy.retracer;

import org.dimyriy.retracer.configuration.ApplicationConfiguration;
import org.dimyriy.retracer.configuration.cli.CliApplicationConfiguration;
import org.dimyriy.retracer.impl.Retracer;

import javax.annotation.Nonnull;

public class Main {
  @SuppressWarnings({"AccessStaticViaInstance", "ConstantConditions"})
  public static void main(@Nonnull final String[] args) {
    final CliApplicationConfiguration cliApplicationConfiguration = new CliApplicationConfiguration();
    new Retracer(createConfigOrExitOnError(cliApplicationConfiguration, args)).run();
  }

  private static ApplicationConfiguration createConfigOrExitOnError(@Nonnull final CliApplicationConfiguration cliApplicationConfiguration,
                                                                    @Nonnull final String[] args) {
    try {
      return cliApplicationConfiguration.parseCli(args);
    } catch (CliApplicationConfiguration.InvalidException e) {
      System.err.println(e.getMessage());
      cliApplicationConfiguration.printUsage();
      System.exit(-1);
      return null;
    }
  }
}
