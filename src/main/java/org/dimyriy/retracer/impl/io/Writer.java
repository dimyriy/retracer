package org.dimyriy.retracer.impl.io;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.dimyriy.retracer.configuration.api.OutputWriterConfiguration;
import org.dimyriy.retracer.impl.aux.StatisticsCounter;
import org.dimyriy.retracer.impl.aux.StatisticsProvider;
import org.dimyriy.retracer.impl.model.TraceRoot;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;
import java.io.*;
import java.time.temporal.TemporalAccessor;
import java.util.Objects;

/**
 * @author Dmitrii Bogdanov
 * Created at 10.05.18
 */
@NotThreadSafe
public class Writer implements AutoCloseable, StatisticsProvider {
  private final BufferedWriter outputStreamWriter;
  private final OutputWriterConfiguration config;
  private final ObjectMapper mapper = createObjectMapper();
  private final StatisticsCounter writtenLines = new StatisticsCounter("TraceWritter");

  public Writer(@Nonnull final OutputWriterConfiguration config) {
    this.config = config;
    if (config.isWriteOutputToStdout()) {
      outputStreamWriter = new BufferedWriter(new OutputStreamWriter(System.out, config.getOutputCharset()));
    } else if (config.isWriteOutputToFile()) {
      final String filename = config.writeOutputToFilename();
      final File file = new File(Objects.requireNonNull(filename));
      try {
        outputStreamWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), config.getOutputCharset()));
      } catch (FileNotFoundException e) {
        throw new OutputFileNotFoundException(filename, e);
      }
    } else {
      throw new IllegalArgumentException("Output is unspecified");
    }
  }

  public void bind() {
    config.bindToWriter(this);
  }

  public void write(@Nonnull final TraceRoot line) {
    if (config == null) {
      throw new IllegalStateException("Not bound to configuration");
    }
    try {
      outputStreamWriter.write(mapper.writeValueAsString(line));
      outputStreamWriter.write('\n');
      writtenLines.increment();
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

  @Override
  public void close() {
    try {
      outputStreamWriter.flush();
      outputStreamWriter.close();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      unbind();
    }
  }

  @Override
  public String getStatistics() {
    return "{written lines: " + writtenLines.toString() + "}";
  }

  @Nonnull
  private ObjectMapper createObjectMapper() {
    final ObjectMapper objectMapper = new ObjectMapper();
    final SimpleModule module = new SimpleModule();
    module.addSerializer(TemporalAccessor.class, new JsonSerializer<TemporalAccessor>() {
      @Override
      public void serialize(final TemporalAccessor value, final JsonGenerator gen, final SerializerProvider serializers) throws IOException {
        gen.writeString(config.threadSafeOutputDateFormatter().format(value));
      }
    });
    objectMapper.registerModule(module);
    return objectMapper;
  }

  private void unbind() {
    config.bindToWriter(null);
  }

  class OutputFileNotFoundException extends RuntimeException {
    OutputFileNotFoundException(@Nonnull final String filename, final Throwable cause) {
      super("File to write traces JSON not found '" + filename + "'", cause);
    }
  }
}
