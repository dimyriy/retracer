package org.dimyriy.retracer.impl.io;

import org.dimyriy.retracer.configuration.api.InputReaderConfiguration;
import org.dimyriy.retracer.impl.aux.StatisticsCounter;
import org.dimyriy.retracer.impl.aux.StatisticsProvider;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

/**
 * @author Dmitrii Bogdanov
 * Created at 10.05.18
 */
@NotThreadSafe
public class Reader implements AutoCloseable, StatisticsProvider {
  private static final int CHUNK_SIZE = 100;
  private final BufferedReader reader;
  private final InputReaderConfiguration config;
  private final StatisticsCounter readLines = new StatisticsCounter("LogReader");

  public Reader(@Nonnull final InputReaderConfiguration config) {
    this.config = config;
    if (config.isReadInputFromStdin()) {
      reader = new BufferedReader(new InputStreamReader(System.in, config.getInputCharset()));
    } else if (config.isReadFromFile()) {
      final String filename = config.inputFilename();
      final File file = new File(Objects.requireNonNull(filename));
      try {
        reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), config.getInputCharset()));
      } catch (FileNotFoundException e) {
        throw new InputFileNotFoundException(filename, e);
      }
    } else {
      throw new IllegalArgumentException("Input is unspecified");
    }
  }

  public void bind() {
    config.bindToReader(this);
  }

  public void submitForAllLinesAndContinue(@Nonnull final Function<List<String>, Void> v, @Nonnull final Runnable continuation) throws IOException {
    checkGuard();
    final List<String> chunks = new ArrayList<>();
    int counter = 0;
    String logLine;
    while ((logLine = reader.readLine()) != null) {
      chunks.add(logLine);
      readLines.increment();
      if (counter == CHUNK_SIZE) {
        counter = 0;
        v.apply(new ArrayList<>(chunks));
        chunks.clear();
      }
      counter++;
    }
    if (!chunks.isEmpty()) {
      v.apply(new ArrayList<>(chunks));
      chunks.clear();
    }
    continuation.run();
  }

  @Override
  public void close() {
    try {
      this.reader.close();
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      unbind();
    }
  }

  @Override
  public String getStatistics() {
    return "{read lines:" + readLines.toString() + "}";
  }

  private void unbind() {
    config.bindToReader(null);
  }

  private void checkGuard() {
    if (config == null) {
      throw new IllegalStateException("Not bound to configuration");
    }
  }

  class InputFileNotFoundException extends RuntimeException {
    InputFileNotFoundException(@Nonnull final String filename, final Throwable cause) {
      super("File to read logs not found '" + filename + "'", cause);
    }
  }
}
