package org.dimyriy.retracer.impl.io;

import com.google.common.base.Stopwatch;
import org.apache.commons.io.output.NullOutputStream;
import org.dimyriy.retracer.configuration.api.StatisticsWriterConfiguration;
import org.dimyriy.retracer.impl.aux.StatisticsProvider;

import javax.annotation.Nonnull;
import java.io.*;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Collectors;

/**
 * @author Dmitrii Bogdanov
 * Created at 10.05.18
 */
public class StatWriter implements AutoCloseable {
  private final StatisticsWriterConfiguration config;
  private final BufferedWriter writer;
  private final TimerTask timerTask;
  private final List<StatisticsProvider> statisticsProviders;
  private final Stopwatch stopwatch = Stopwatch.createUnstarted();

  public StatWriter(@Nonnull final StatisticsWriterConfiguration config, @Nonnull final List<StatisticsProvider> statisticsProviders) {
    this.config = config;
    this.statisticsProviders = statisticsProviders;
    timerTask = new TimerTask() {
      @Override
      public void run() {
        write(calculateStatistics());
      }
    };
    writer = createWriter(config);
  }

  public void bindAndStart() {
    config.bindToStatisticsWriter(this);
    stopwatch.start();
    new Timer().schedule(timerTask, config.outputStatisticsIntervalMs(), config.outputStatisticsIntervalMs());
  }

  @Override
  public void close() {
    try {
      write("Statistics for the whole run: " + calculateStatistics());
      timerTask.cancel();
      stopwatch.stop();
      writer.close();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      unbind();
    }
  }

  private String calculateStatistics() {
    return statisticsProviders.stream().filter(Objects::nonNull).map(StatisticsProvider::getStatistics).collect(Collectors.joining(" | "));
  }

  private BufferedWriter createWriter(@Nonnull final StatisticsWriterConfiguration config) {
    if (config.isRuntimeStatisticsWriterEnabled() || config.isTracesStatisticsWriterEnabled()) {
      if (config.isWriteStatisticsToStderr()) {
        return new BufferedWriter(new OutputStreamWriter(System.err));
      } else {
        final String filename = config.writeStatisticsToFilename();
        final File file = new File(Objects.requireNonNull(filename));
        try {
          return new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
        } catch (FileNotFoundException e) {
          throw new OutputFileNotFoundException(filename, e);
        }
      }
    } else {
      return new BufferedWriter(new OutputStreamWriter(NullOutputStream.NULL_OUTPUT_STREAM));
    }
  }

  private void write(@Nonnull final String line) {
    if (config == null) {
      throw new IllegalStateException("Not bound to configuration");
    }
    try {
      writer.write(line + "\n");
      writer.flush();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void unbind() {
    config.bindToStatisticsWriter(null);
  }


  class OutputFileNotFoundException extends RuntimeException {
    OutputFileNotFoundException(@Nonnull final String filename, final Throwable cause) {
      super("Problem creating file to write statistics to '" + filename + "'", cause);
    }
  }
}
