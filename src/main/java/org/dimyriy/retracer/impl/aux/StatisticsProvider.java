package org.dimyriy.retracer.impl.aux;

/**
 * @author Dmitrii Bogdanov
 * Created at 13.05.18
 */
public interface StatisticsProvider {
  String getStatistics();
}
