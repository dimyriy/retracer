package org.dimyriy.retracer.impl.aux;

import com.google.common.base.Stopwatch;

import javax.annotation.Nonnull;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Dmitrii Bogdanov
 * Created at 13.05.18
 */
public class StatisticsCounter {
  @SuppressWarnings("FieldCanBeLocal")
  private final String name;
  private final AtomicInteger counter = new AtomicInteger(0);
  private final Stopwatch stopwatch = Stopwatch.createUnstarted();

  public StatisticsCounter(@Nonnull final String name) {
    this.name = name;
  }

  public void incrementBy(final int count) {
    if (!stopwatch.isRunning()) {
      stopwatch.start();
    }
    counter.addAndGet(count);
  }

  public void increment() {
    if (!stopwatch.isRunning()) {
      stopwatch.start();
    }
    counter.incrementAndGet();
  }

  @Override
  public String toString() {
    final int count = counter.get();
    final double v = stopwatch.isRunning() ? (count * 1000_000 + 0.) / stopwatch.elapsed().get(ChronoUnit.NANOS) : 0.0;
    return counter.get() + ", rate: " + v + "op/ms";
  }
}
