package org.dimyriy.retracer.impl;

import org.dimyriy.retracer.configuration.ApplicationConfiguration;
import org.dimyriy.retracer.impl.io.Reader;
import org.dimyriy.retracer.impl.io.StatWriter;
import org.dimyriy.retracer.impl.io.Writer;
import org.dimyriy.retracer.impl.model.LogLine;
import org.dimyriy.retracer.impl.model.LogLineParser;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author Dmitrii Bogdanov
 * Created at 09.05.18
 */
public class Retracer {
  private final ApplicationConfiguration config;
  private final LogLineParser parser;
  private final Trace trace;
  private final ExecutorService executorService;

  public Retracer(@Nonnull final ApplicationConfiguration config) {
    this.config = config;
    parser = new LogLineParser(config.threadSafeInputDateFormatter());
    trace = new Trace(config);
    if (config.useMultipleThreads()) {
      executorService = Executors.newFixedThreadPool(config.concurrencyLevel());
    } else {
      executorService = Executors.newSingleThreadExecutor();
    }

  }

  public void run() {
    try (final Reader reader = new Reader(config);
         final Writer writer = new Writer(config);
         final StatWriter statWriter = new StatWriter(config, Arrays.asList(reader, writer, parser, trace))) {
      reader.bind();
      writer.bind();
      statWriter.bindAndStart();
      try {
        reader.submitForAllLinesAndContinue(logLinesChunk -> {
          executorService.submit(() -> logLinesChunk.forEach((line) -> {
            final LogLine parsed = parser.parse(line);
            if (parsed != null) {
              trace.addLogLine(parsed);
            }
          }));
          return null;
        }, () -> {
          executorService.shutdown();
          try {
            executorService.awaitTermination(1L, TimeUnit.HOURS);
            trace.asRoots().forEach(writer::write);
          } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
          }
        });
      } catch (IOException e) {
        e.printStackTrace();
        throw new RuntimeException(e);
      }
    }
  }
}
