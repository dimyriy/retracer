package org.dimyriy.retracer.impl.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * @author Dmitrii Bogdanov
 * Created at 13.05.18
 */
@SuppressWarnings("FieldMayBeFinal")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class TraceRoot {
  private String id;
  private LogLine root;

  public TraceRoot(final String id, final LogLine root) {
    this.id = id;
    this.root = root;
  }

  public String getId() {
    return id;
  }

  public LogLine getRoot() {
    return root;
  }
}
