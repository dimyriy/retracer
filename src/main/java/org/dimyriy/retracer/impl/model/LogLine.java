package org.dimyriy.retracer.impl.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.annotation.Nonnull;
import java.time.temporal.TemporalAccessor;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Dmitrii Bogdanov
 * Created at 10.05.18
 */
@SuppressWarnings("unused")
public class LogLine {
  private final TemporalAccessor start;
  private final TemporalAccessor end;
  @JsonIgnore
  private final String trace;
  private final String service;
  @JsonIgnore
  private final String callerSpan;
  private final String span;
  private final Set<LogLine> calls = ConcurrentHashMap.newKeySet();

  LogLine(@Nonnull final TemporalAccessor startTimestamp,
          @Nonnull final TemporalAccessor endTimestamp,
          @Nonnull final String trace,
          @Nonnull final String serviceName,
          @Nonnull final String callerSpan,
          @Nonnull final String span) {
    this.start = startTimestamp;
    this.end = endTimestamp;
    this.trace = trace;
    this.service = serviceName;
    this.callerSpan = callerSpan;
    this.span = span;
  }

  public Set<LogLine> getCalls() {
    return calls;
  }


  public String getCallerSpan() {
    return callerSpan;
  }

  public String getSpan() {
    return span;
  }

  public String getService() {
    return service;
  }

  public String getTrace() {
    return trace;
  }

  public TemporalAccessor getStart() {
    return start;
  }

  public TemporalAccessor getEnd() {
    return end;
  }

  @JsonIgnore
  public boolean isRoot() {
    return "null".equals(callerSpan);
  }

  public void addChild(@Nonnull final LogLine logLine) {
    calls.add(logLine);
  }
}
