package org.dimyriy.retracer.impl.model;

import org.apache.commons.lang3.StringUtils;
import org.dimyriy.retracer.impl.aux.StatisticsCounter;
import org.dimyriy.retracer.impl.aux.StatisticsProvider;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.time.format.DateTimeFormatter;

/**
 * @author Dmitrii Bogdanov
 * Created at 10.05.18
 */
@ThreadSafe
public class LogLineParser implements StatisticsProvider {
  private final DateTimeFormatter dateFormat;
  private final StatisticsCounter errorCount = new StatisticsCounter("logLineParser");

  public LogLineParser(@Nonnull final DateTimeFormatter dateFormat) {
    this.dateFormat = dateFormat;
  }

  @Override
  public String getStatistics() {
    return "{Malformed lines:" + errorCount.toString() + "}";
  }

  public LogLine parse(@Nonnull final String line) {
    try {
      final String[] tokenized = StringUtils.split(line, StringUtils.SPACE);
      if (tokenized == null || tokenized.length != 5) {
        throw new LogParseException("Malformed logline");
      }
      final String[] span = StringUtils.split(tokenized[4], "->");
      if (span == null || span.length != 2) {
        throw new LogParseException("Malformed span in logline");
      }
      return new LogLine(dateFormat.parse(tokenized[0]), dateFormat.parse(tokenized[1]), tokenized[2], tokenized[3], span[0], span[1]);
    } catch (LogParseException e) {
      e.printStackTrace();
      errorCount.increment();
      return null;
    }
  }

  class LogParseException extends RuntimeException {
    LogParseException(final String message) {
      super(message);
    }
  }
}
