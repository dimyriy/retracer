package org.dimyriy.retracer.impl;

import org.dimyriy.retracer.configuration.ApplicationConfiguration;
import org.dimyriy.retracer.impl.aux.StatisticsCounter;
import org.dimyriy.retracer.impl.aux.StatisticsProvider;
import org.dimyriy.retracer.impl.model.LogLine;
import org.dimyriy.retracer.impl.model.TraceRoot;

import javax.annotation.Nonnull;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

/**
 * @author Dmitrii Bogdanov
 * Created at 13.05.18
 */
public class Trace implements StatisticsProvider {
  private final Map<String, LogLine> traceToRoot = new ConcurrentHashMap<>();
  private final Map<String, LogLine> traceCallerToLogLines = new ConcurrentHashMap<>();
  private final Set<LogLine> orphanedLines = ConcurrentHashMap.newKeySet();
  private final StatisticsCounter droppedOrphanedLinesCount = new StatisticsCounter("Traces stats");
  private final ApplicationConfiguration config;
  @SuppressWarnings("FieldCanBeLocal")
  private final Timer orphanedLinesGarbageCollector = new Timer();
  private volatile Instant latestLogTime = Instant.ofEpochMilli(0L);

  public Trace(@Nonnull final ApplicationConfiguration config) {
    this.config = config;
    orphanedLinesGarbageCollector.schedule(new TimerTask() {
      @Override
      public void run() {
        tryToReconstructOrphanedLines();
      }
    }, 100L, 100L);
  }

  @Override
  public String getStatistics() {
    if (config.isTracesStatisticsWriterEnabled()) {
      return "{Drop orphaned lines: " + droppedOrphanedLinesCount.toString() + "; roots count:" + traceToRoot.size() + "}";
    } else {
      return null;
    }
  }

  private void tryToReconstructOrphanedLines() {
    synchronized (orphanedLines) {
      final Set<LogLine> orphanedLinesCopy = new HashSet<>(orphanedLines);
      orphanedLines.clear();
      for (final LogLine logLine : orphanedLinesCopy) {
        addLogLine(logLine);
      }
    }
  }

  void addLogLine(@Nonnull final LogLine logLine) {
    if (logLine.isRoot()) {
      addRoot(logLine);
    } else {
      addChild(logLine);
    }
  }

  Stream<TraceRoot> asRoots() {
    while (!orphanedLines.isEmpty()) {
      final int previousIndexSize = traceCallerToLogLines.size();
      tryToReconstructOrphanedLines();
      if (previousIndexSize == traceCallerToLogLines.size()) {
        break;
      }
    }
    droppedOrphanedLinesCount.incrementBy(orphanedLines.size());
    return traceToRoot.values().stream().map(root -> new TraceRoot(root.getTrace(), root));
  }

  private void addRoot(@Nonnull final LogLine logLine) {
    if (traceToRoot.putIfAbsent(logLine.getTrace(), logLine) != null) {
      throw new IllegalStateException("Root with caller span " + logLine.getSpan() + " already exists. Looks like trace is invalid");
    }
    addToIndex(logLine);
  }

  private void addChild(@Nonnull final LogLine logLine) {
    final LogLine root = traceToRoot.get(logLine.getTrace());
    if (root != null) {
      final LogLine caller = getCallerFromIndex(logLine);
      if (caller != null) {
        caller.addChild(logLine);
        addToIndex(logLine);
        return;
      }
    }
    addOrphanedLine(logLine);
  }

  private void addOrphanedLine(@Nonnull final LogLine logLine) {
    synchronized (orphanedLines) {
      if (shouldDropOrphanedLine(logLine)) {
        droppedOrphanedLinesCount.increment();
      } else {
        orphanedLines.add(logLine);
      }
    }
  }

  private void addToIndex(@Nonnull final LogLine logLine) {
    final Instant logTime = Instant.from(logLine.getEnd());
    if (latestLogTime.isBefore(logTime)) {
      latestLogTime = logTime;
    }
    if (traceCallerToLogLines.putIfAbsent(buildIndexKey(logLine.getTrace(), logLine.getSpan()), logLine) != null) {
      throw new IllegalStateException("Duplicated call");
    }
  }

  private LogLine getCallerFromIndex(@Nonnull final LogLine logLine) {
    return traceCallerToLogLines.get(buildIndexKey(logLine.getTrace(), logLine.getCallerSpan()));
  }

  @Nonnull
  private String buildIndexKey(@Nonnull final String trace, @Nonnull final String span) {
    return trace + ":" + span;
  }

  private boolean shouldDropOrphanedLine(@Nonnull final LogLine orphanedLine) {
    return Instant.from(orphanedLine.getEnd()).isBefore(latestLogTime.minus(config.dropOrphanedLinesAfterTimeoutMs(), ChronoUnit.MILLIS));
  }
}