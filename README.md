# Retracer 
Trace reconstruction from logs for distributed systems

[![pipeline status](https://gitlab.com/dimyriy/retracer/badges/master/pipeline.svg)](https://gitlab.com/dimyriy/retracer/commits/master)
[![coverage report](https://gitlab.com/dimyriy/retracer/badges/master/coverage.svg)](https://gitlab.com/dimyriy/retracer/commits/master)
[![TeamCity](https://teamcity.jetbrains.com/app/rest/builds/buildType:(id:TestDrive_Retracer_Build)/statusIcon)](https://teamcity.jetbrains.com/project.html?projectId=TestDrive_Retracer)

## Build:
`./gradlew clean jar`

## Usage:
`# java -jar build/libs/retracer-1.0.1-RELEASE.jar --help`

